# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
%define crh_dir /usr/%{crh_tag}/%{crh_version_with_bn} 
%define tez_name tez
%define tez_home %{crh_dir}/%{tez_name}
%define lib_tez %{tez_home}/lib
%define man_dir %{_mandir}


%if %{!?suse_version:1}0 && %{!?mgaversion:1}0

%define __os_install_post \
    %{_rpmconfigdir}/brp-compress ; \
    %{_rpmconfigdir}/brp-strip-static-archive %{__strip} ; \
    %{_rpmconfigdir}/brp-strip-comment-note %{__strip} %{__objdump} ; \
    /usr/lib/rpm/brp-python-bytecompile ; \
    %{nil}

%define doc_tez %{_docdir}/tez-%{tez_version}

%endif


%if  %{?suse_version:1}0

# Only tested on openSUSE 11.4. le'ts update it for previous release when confirmed
%if 0%{suse_version} > 1130
%define suse_check \# Define an empty suse_check for compatibility with older sles
%endif

%define doc_tez %{_docdir}/tez
%define alternatives_cmd update-alternatives
%define __os_install_post \
    %{suse_check} ; \
    /usr/lib/rpm/brp-compress ; \
    %{nil}

%endif

Name: tez%{crh_version_as_name}
Version: %{tez_version}
Release: %{tez_release}
Summary:Apache Tez is the Hadoop enhanced Map/Reduce module.
URL: http://tez.apache.org
Group: Development/Libraries
Buildroot: %{_topdir}/INSTALL/%{tez_name}-%{version}
License: Apache License v2.0
Source0: apache-%{tez_name}-%{tez_base_version}-src.tar.gz
Source1: do-component-build
Source2: install_tez.sh
Source3: tez.1
Source4: tez-site.xml
Source5: bigtop.bom
Source6: init.d.tmpl
BuildArch: noarch
Requires: hadoop%{crh_version_as_name} hadoop%{crh_version_as_name}-hdfs hadoop%{crh_version_as_name}-yarn hadoop%{crh_version_as_name}-mapreduce
Requires: ambari-mpacks%{crh_version_as_name}-crh-DW

#Patch1: patch1-bower-allow-root.diff


%if  0%{?mgaversion}
Requires: bsh-utils
%else
Requires: sh-utils
%endif


%description
The Apache Tez project is aimed at building an application framework
which allows for a complex directed-acyclic-graph of tasks for
processing data. It is currently built atop Apache Hadoop YARN

%prep
%setup -q -n apache-%{tez_name}-%{tez_base_version}-src
#%patch1 -p1


%build
env TEZ_VERSION=%{version} tez_name=%{tez_name} tez_base_version=%{tez_base_version} bash %{SOURCE1}

%install
%__rm -rf $RPM_BUILD_ROOT

cp %{SOURCE3} %{SOURCE4} .
env CRH_DIR=%{crh_dir} CRH_VERSION=%{crh_version_with_bn} TEZ_BASE_VERSION=%{tez_base_version} sh %{SOURCE2} \
        --build-dir=. \
        --doc-dir=%{doc_tez} \
        --libexec-dir=%{libexec_tez} \
	    --prefix=$RPM_BUILD_ROOT

%__ln_s -f %{crh_dir}/hadoop/hadoop-annotations.jar $RPM_BUILD_ROOT/%{lib_tez}/hadoop-annotations.jar
%__ln_s -f %{crh_dir}/hadoop/hadoop-auth.jar $RPM_BUILD_ROOT/%{lib_tez}/hadoop-auth.jar
%__ln_s -f %{crh_dir}/hadoop-mapreduce/hadoop-mapreduce-client-common.jar $RPM_BUILD_ROOT/%{lib_tez}/hadoop-mapreduce-client-common.jar
%__ln_s -f %{crh_dir}/hadoop-mapreduce/hadoop-mapreduce-client-core.jar $RPM_BUILD_ROOT/%{lib_tez}/hhadoop-mapreduce-client-core.jar
%__ln_s -f %{crh_dir}/hadoop-yarn/hadoop-yarn-server-web-proxy.jar $RPM_BUILD_ROOT/%{lib_tez}/hadoop-yarn-server-web-proxy.jar

%pre

%post
ln -s /etc/tez/conf %{tez_home}/conf
%preun

#######################
#### FILES SECTION ####
#######################
%files
%defattr(-,root,root)
%{tez_home}
%doc %{doc_tez}
%{man_dir}/man1/tez.1.*
/etc/tez/conf/tez-site.xml
