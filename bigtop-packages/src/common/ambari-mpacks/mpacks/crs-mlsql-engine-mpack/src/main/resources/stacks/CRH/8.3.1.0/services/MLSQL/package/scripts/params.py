#!/usr/bin/env python

from resource_management.libraries.script import Script
from resource_management.libraries.functions import default

# server configurations
config = Script.get_config()
stack_root = Script.get_stack_root()

mlsql_user = config['configurations']['mlsql-env']['mlsql_user']
user_group = config['configurations']['cluster-env']['user_group']
log_dir = config['configurations']['mlsql-env']['mlsql_log_dir']
mysql_user = config['configurations']['mlsql-env']['mysql_user']
mysql_password = config['configurations']['mlsql-env']['mysql_password']

engine_conf = config['configurations']['mlsql-env']['engine_conf']
cluster_conf = config['configurations']['mlsql-env']['cluster_conf']

hostname = config['agentLevelParams']['hostname']
spark_home = config['ambariLevelParams']['spark_home']

install_dir_engine = stack_root + '/mlsql-engine'
download_url_engine = config['configurations']['mlsql-env']['download_url_engine']
filename_engine = download_url_engine.split('/')[-1]
version_dir_engine = filename_engine.replace('.tar.gz', '').replace('.tgz', '')

install_dir_cluster = stack_root + '/mlsql-cluster'
download_url_cluster = config['configurations']['mlsql-env'][
    'download_url_cluster']
filename_cluster = download_url_cluster.split('/')[-1]
version_dir_cluster = filename_cluster.replace('.tar.gz', '').replace(
    '.tgz', '')

install_dir_console = stack_root + '/mlsql-console'
download_url_console = config['configurations']['mlsql-env'][
    'download_url_console']
filename_console = download_url_console.split('/')[-1]
version_dir_console = filename_console.replace('.tar.gz', '').replace(
    '.tgz', '')

conf_dir = '/etc/mlsql'

mlsql_env_content = config['configurations']['mlsql-env'][
    'mlsql_env_content']

engine_conf = config['configurations']['mlsql-env'][
    'engine_conf']


mlsqlengine_hosts = default("/clusterHostInfo/mlsql_engine_hosts", [])
mlsqlcluster_hosts = default("/clusterHostInfo/mlsql_cluster_hosts", [])

frontend_env = 'PLAY_BINARY_OPTS="-Dconfig.file=/etc/wherehows/frontend/application.conf -Dhttp.address={{hostname}} -Dhttp.port=9001"'
frontend_systemd = '''
[Unit]
Description=Wherehows Frontend
After=network.target

[Service]
EnvironmentFile=-/etc/sysconfig/wherehows_frontend
PIDFile=/var/run/wherehows/frontend.pid
WorkingDirectory={{install_dir}}
ExecStart={{install_dir}}/bin/playBinary
Restart=on-failure
User={{wherehows_user}}
Group={{user_group}}
SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
'''

backend_env = 'PLAY_BINARY_OPTS="-Dconfig.file=/etc/wherehows/backend/application.conf  -Dhttp.address={{hostname}} -Dhttp.port=19001"'

backend_systemd = '''
[Unit]
Description=Wherehows Backend
After=network.target

[Service]
EnvironmentFile=-/etc/sysconfig/wherehows_backend
PIDFile=/var/run/wherehows/backend.pid
WorkingDirectory={{install_dir_backend}}
ExecStart={{install_dir_backend}}/bin/playBinary
Restart=on-failure
User={{wherehows_user}}
Group={{user_group}}

SuccessExitStatus=143

[Install]
WantedBy=multi-user.target
'''
