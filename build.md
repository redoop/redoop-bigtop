## 编译方法  
https://cwiki.apache.org/confluence/display/BIGTOP/Bigtop+CI+Setup+Guide

 ```
 
## Name: BUILD_ENVIRONMENTS

Values: 
centos-6
centos-7
fedora-25
ubuntu-16.04
debian-8
opensuse-42.1

## Name: COMPONENTS

Values: 
bigtop-groovy
bigtop-jsvc
bigtop-tomcat
bigtop-utils
crunch
datafu
flume
giraph
hadoop
hama
hbase
hive
hue
ignite-hadoop
kafka
kite
mahout
oozie
phoenix
pig
solr
spark
sqoop
sqoop2
tachyon
tez
ycsb
zookeeper

docker run --rm -v `pwd`:/ws --workdir /ws bigtop/slaves:trunk-$BUILD_ENVIRONMENTS bash -l -c './gradlew allclean $COMPONENTS-pkg'


```

